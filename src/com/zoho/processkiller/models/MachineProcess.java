package com.zoho.processkiller.models;

/**
 * Model for Machine Process. 
 * @author rbalamurugan
 *
 */
public class MachineProcess {
	
	private int pid;
	private String command;
	private String name;
	
	public MachineProcess(int pid, String command){
		this.pid = pid;
		this.command = command;
	}
	
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
