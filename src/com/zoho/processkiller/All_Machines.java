package com.zoho.processkiller;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.zoho.processkiller.adapter.Machine;
import com.zoho.processkiller.adapter.MachineDbAdapter;

public class All_Machines extends Activity {

	private MachineDbAdapter dbHelper;
	private SimpleCursorAdapter dataAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		dbHelper = new MachineDbAdapter(this);

		// dbHelper.deleteAllMachine();

		// defaultViews();

		displayMachineListView();

	}

	private void displayMachineListView() {

		Cursor cursor = dbHelper.getAllMachines();

		String[] colums = new String[] { MachineDbAdapter.KEY_HOSTNAME,
				MachineDbAdapter.KEY_USERNAME, };

		int[] to = new int[] { R.id.hostname, R.id.username, };

		dataAdapter = new SimpleCursorAdapter(this,
				R.layout.activity_all_machines, cursor, colums, to, 0);

		ListView listView = (ListView) findViewById(R.id.listView1);

		listView.setAdapter(dataAdapter);

	}

	private void defaultViews() {

		dbHelper.addMachine(new Machine("rbalamurugan", "bala-1030", "Bala@88"));
		dbHelper.addMachine(new Machine("gramkumar-0786", "gramkumar-0786",
				"gram@88"));
		dbHelper.addMachine(new Machine("karthi-0878", "karthi", "kutty@2"));
		dbHelper.addMachine(new Machine("ram-1045", "ram", "ram123"));
	}

	private void defaultJson() {

		String processdata = "{ \"5884\": \"ssh\", \"10050\": \"ssh\", \"14843\": \"man\",	\"14854\": \"pager\" }";
		try {
			JSONObject process = new JSONObject(processdata);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.all__machines, menu);
		return true;
	}

	public static void show(Context ctxt) {
		Intent intent = new Intent(ctxt, All_Machines.class);
		ctxt.startActivity(intent);
	}

}
