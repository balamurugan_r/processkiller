package com.zoho.processkiller;

import java.util.List;


import com.zoho.processkiller.adapter.MachineDbAdapter;
import com.zoho.processkiller.adapter.Machine;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class Add_Machine extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_machine);
  
	}
	
	public void showAllMachines(View view){
		
		EditText hostnameText = (EditText)findViewById(R.id.hostname);
		EditText usernameText = (EditText)findViewById(R.id.username);
		EditText passwordText = (EditText)findViewById(R.id.password);
		
		String hostname = hostnameText.getText().toString();
		String username = usernameText.getText().toString();
		String password = passwordText.getText().toString();

		
		MachineDbAdapter dbHelper = new MachineDbAdapter(this);
        
		dbHelper.addMachine(new Machine(hostname,username ,password));   
		
		Intent intent = new Intent(this, All_Machines.class);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add__machine, menu);
		return true;
	}

}
