package com.zoho.processkiller.adapter;

/**
 * Model for Machine details.
 * @author bala-1030
 *
 */
public class Machine {

	private int id;
	private String hostname;
	private String username;
	private String password;
	
	public Machine(int id){
		this.id = id;
	}
	
	public Machine(int id, String hostname, String username, String password){
		this.id = id;
		this.hostname = hostname;
		this.username = username;
		this.password = password;
	}
	public Machine(String hostname, String username, String password){
		this.id = id;
		this.hostname = hostname;
		this.username = username;
		this.password = password;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
