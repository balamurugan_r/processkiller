package com.zoho.processkiller.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.webkit.WebChromeClient.CustomViewCallback;

public class MachineDbAdapter  {

	 private static final int DATABASE_VERSION = 1;
	 
	// Database Name
	private static final String DATABASE_NAME = "ProcessTracker";
	 
		// Machine table name
	private static final String TABLE_MACHINE = "machine";
	 
	    // Contacts Table Columns names
	public static final String KEY_ID = "_id";
	public static final String KEY_HOSTNAME = "hostname";
	public static final String KEY_USERNAME = "username";
	public static final String KEY_PASSWORD = "password";
	
	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;
	private final Context mCtx;
	
	
	private static class DatabaseHelper extends SQLiteOpenHelper { 
	
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	

	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_MACHINE + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_HOSTNAME + " TEXT,"
                + KEY_USERNAME + " TEXT," +KEY_PASSWORD + " TEXT"+ ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MACHINE);
		
		// Create tables again
		onCreate(db);
	}
	}
	
	public MachineDbAdapter(Context context){	
		this.mCtx = context;
		this.mDbHelper = new DatabaseHelper(mCtx);
	}
	
	public void addMachine(Machine machine){
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		 
		ContentValues values = new ContentValues();
		values.put(KEY_HOSTNAME, machine.getHostname()); 
		values.put(KEY_USERNAME, machine.getUsername()); 
		values.put(KEY_PASSWORD, machine.getPassword()); 

		    // Inserting Row
		db.insert(TABLE_MACHINE, null, values);
		db.close(); // Closing database connection
	}
	
	public Cursor getMachine(int id){
	    SQLiteDatabase db = mDbHelper.getReadableDatabase();
	    
	    Cursor cursor = db.query(TABLE_MACHINE, new String[] { KEY_ID,
	            KEY_HOSTNAME, KEY_USERNAME, KEY_PASSWORD }, KEY_ID + "=?",
	            new String[] { String.valueOf(id) }, null, null, null, null);
	    if (cursor != null){
	        cursor.moveToFirst();
	    }
	    return cursor;
	}
	
	public Cursor getAllMachines(){
	    List<Machine> machineList = new ArrayList<Machine>();
	    // Select All Query
	    String selectQuery = "SELECT  * FROM " + TABLE_MACHINE;
	 
	    SQLiteDatabase db = mDbHelper.getReadableDatabase();
	    Cursor cursor = db.rawQuery(selectQuery, null);
	 
	    // looping through all rows and adding to list
	    if (cursor == null){
	    	cursor.moveToFirst();
	    }
	    return cursor;
	    }
	
	public int getMachineCount(){
		
	    String selectQuery = "SELECT  * FROM " + TABLE_MACHINE;
	    SQLiteDatabase db = mDbHelper.getReadableDatabase();
	    Cursor cursor = db.rawQuery(selectQuery, null);
	    cursor.close();
	    
		return cursor.getCount();
		}
	
	public int updateMachine(Machine machine){
		
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		
		ContentValues values = new ContentValues();
	    values.put(KEY_HOSTNAME, machine.getHostname());
	    values.put(KEY_USERNAME, machine.getUsername());
	    values.put(KEY_PASSWORD, machine.getPassword());
	    
	    return db.update(TABLE_MACHINE, values, KEY_ID + " = ?",
	            new String[] { String.valueOf(machine.getId()) });	
	}
	
	public void deleteMachine(Machine machine){
	
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		db.delete(TABLE_MACHINE, KEY_ID + " = ?",
		            new String[] { String.valueOf(machine.getId()) });
		db.close();
	}
	
	public boolean deleteAllMachine(){
		int doneDelete = 0;
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		doneDelete = db.delete(TABLE_MACHINE, null , null);
		db.close();
	return doneDelete > 0;
	}
	

}
