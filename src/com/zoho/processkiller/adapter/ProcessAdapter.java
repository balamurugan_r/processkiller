package com.zoho.processkiller.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zoho.processkiller.R;
import com.zoho.processkiller.models.MachineProcess;

public class ProcessAdapter extends ArrayAdapter<MachineProcess>{
	
	
	private  Context context;
	private List<MachineProcess> objects=null;
	private int layoutId = -1;
	private LayoutInflater inflater = null;
	
	public ProcessAdapter(Context context, int resource,int textViewResourceId, List<MachineProcess> objects)
	{
		super(context, resource, textViewResourceId, objects);
		this.context = context;
		this.objects = objects;
		layoutId =resource;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	static class ViewHolder
	{
		public TextView pid;
		public TextView cmd;
	}
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View rowView = convertView;
		if(rowView == null)
		{

			rowView = inflater.inflate(layoutId,parent, false);
			ViewHolder holder = new ViewHolder();
			holder.pid = (TextView) rowView.findViewById(R.id.pid);
			holder.cmd =  (TextView) rowView.findViewById(R.id.command);
			rowView.setTag(holder);
		}
		
		MachineProcess mp = objects.get(position);
		ViewHolder holder = (ViewHolder)rowView.getTag();
		holder.pid.setText(mp.getPid()+"");
		holder.cmd.setText(mp.getCommand());
		return rowView;
	}

	
}
