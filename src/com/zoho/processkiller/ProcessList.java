package com.zoho.processkiller;

import java.util.List;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.zoho.processkiller.adapter.ProcessAdapter;
import com.zoho.processkiller.models.MachineProcess;
import com.zoho.processkiller.util.PKUtil;

public class ProcessList extends Activity {

	private ListView listview = null;
	private PullToRefreshListView mPullRefreshListView;
	private List<MachineProcess> processList = null;
	
	private ProcessAdapter adapter = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.process_activity);
//		getLayoutProperties();
		
		mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.processlist);
		mPullRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				String label = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(),
						DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

				// Update the LastUpdatedLabel
				refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);

				// Do work to refresh the list here.
				new GetDataTask().execute();
			}
		});
		
		//showProcess();
		
		ListView actualListView = mPullRefreshListView.getRefreshableView();
		registerForContextMenu(actualListView);
		
		processList = PKUtil.INSTANCE.getProcessList();
		adapter = new ProcessAdapter(this, R.layout.process_item, 0, processList);
//		//listview.setAdapter(adapter);
		
		actualListView.setAdapter(adapter);

	}

	public void getLayoutProperties()
	{
		listview = (ListView)findViewById(R.id.processlist);
	}

	public void onRemoveClicked(View view)
	{
		processList.remove(0);
		adapter.notifyDataSetChanged();
	}
	/*private void showProcess() 
	{

		processList = PKUtil.INSTANCE.getProcessList();
		adapter = new ProcessAdapter(this, R.layout.process_item, 0, processList);
		listview.setAdapter(adapter);
		
		
	}*/
	
	private class GetDataTask extends AsyncTask<Void, Void, List<MachineProcess>> {

		@Override
		protected List<MachineProcess> doInBackground(Void... params) {
			// Simulates a background job.
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
			}
			return processList;
		}

		@Override
		protected void onPostExecute(List<MachineProcess> result) {
			adapter.notifyDataSetChanged();

			// Call onRefreshComplete when the list has been refreshed.
			mPullRefreshListView.onRefreshComplete();

			super.onPostExecute(result);
		}
	}


}
