package com.zoho.processkiller.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.zoho.processkiller.models.MachineProcess;

public enum PKUtil  {
  
	INSTANCE;
	
	private String processData = null;

	public ArrayList<MachineProcess> getProcessList(){
		
		ArrayList<MachineProcess> processList = new ArrayList<MachineProcess>();
		
		String processdata = "{"+
							"\"column_order\":[\"PID\",\"CMD\"],"+
							"\"rows\":["+
										"[\"1343\",\"Zohocrm\"],"+
										"[\"10050\",\"Zohocampaign\"],"+
										"[\"14843\",\"Zohocalendar\"],"+
										"[\"14854\",\"Opmanager\"]" +
									"]"+
							"}";
	
		try {
				
	//		new GetDataTask().execute();
			 JSONObject data = new JSONObject(processdata);
		      JSONArray process_data = data.getJSONArray("rows");
		      
		      int length = process_data.length();
		      for(int i = 0; i < length;i++)
		      {
		    	  
		    	 JSONArray values = process_data.getJSONArray(i);
		    	 
		    	 MachineProcess process = new MachineProcess(values.getInt(0),values.getString(1));
		    	 processList.add(process);
		      }
		    } catch (Exception e) 
		    {
		    	e.printStackTrace();
		    	Log.d(PKUtil.class.getName(), "Parse while process list from json "+e.getMessage());
		    }
		
		return processList;
		
	}
	
	 static InputStream is = null;
	 static JSONObject jObj = null;
	 static String json = "";
	
	public String getJSONFromUrl(String url) {
	
        // Making HTTP request
        try {
            // defaultHttpClient
        	HttpPost docsPost = new HttpPost();
    		URI uriObject = new URI(url);
			
    		docsPost.setURI(uriObject);
    		DefaultHttpClient  httpClient = new DefaultHttpClient();
           
            HttpResponse httpResponse = httpClient.execute(docsPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();           
 
        }
		catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
         
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
 
     /*   // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
 */
        // return JSON String
        return json;
 
    }
	/*private class GetDataTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			processData = getJSONFromUrl("http://bala-1030:8000/process.json");
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
			}
			return processData;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
		}
	}*/
 
} 